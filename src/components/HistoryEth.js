import React from 'react';
import styled from 'styled-components';
import Paragraph from './Paragraph';
import Moment from 'react-moment';
import Loading from './Loading';

Moment.globalFormat = 'D/MMM/YYYY HH:MM';

const Div = styled.div`
    border-top: 1px solid lightgray;
    border-bottom: 1px solid lightgray;
    width: 100%;
    height: 40px;
    font-size: 18px;
    display: flex;
    justify-content: space-between;
    box-sizing: border-box;
    z-index: 10000;
`

function format(str,cut){
    return `${str.substring(0,cut)}...`;
}

function hex2Ether(hex){
    let ethers = parseInt(hex);
    let ether = ethers / 100000000000000000000;
    return ether.toPrecision(2);
}

function HistoryComponent(props){

    return(
        <Div>
            {props.index}.
            <Paragraph title={props.history.hash}>{format(props.history.hash,15)}</Paragraph>
            <Paragraph title={props.history.blockNumber}>{props.history.blockNumber}</Paragraph>
            <Paragraph title={props.history.timestamp}><Moment unix>{props.history.timestamp}</Moment></Paragraph>
            <Paragraph title={props.history.from}>{format(props.history.from,7)}</Paragraph>
            <Paragraph title={props.history.to}>{format(props.history.to,8)}</Paragraph>
            <Paragraph title={props.history.value._hex}>{hex2Ether(props.history.value._hex)} Ethers</Paragraph>
        </Div>
    )
}

const HistoryEth = (props) => {
    const items = [];

    const history = props.history;
    console.log(props.history)
    
    for (let i = 0; i < history.length; i++) {
        items.push(<HistoryComponent index={i+1}history={props.history[i]}/>)
      }
    
    if(!props.history){
        return <Loading />
    }
    
    return(
        <div> 
            {items}
        </div>
    )
}

export default HistoryEth;