import styled from 'styled-components';

const Header = styled.div`
    height:100px;
    width: 100%;
    top:0;
    right: 0;
    background: #2E86AB;
    text-align: center;
    position: fixed;
`

export default Header;