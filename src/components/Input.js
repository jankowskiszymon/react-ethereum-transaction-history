import styled from 'styled-components';

const Input = styled.input`
    margin-top: 20px;
    height: 30px;
    width: 350px;
    color: lightgray;
    outline: none;
    border-radius: 10px;
    border: none;

    :placeholder{
        text-align: center;
    }
`

export default Input;