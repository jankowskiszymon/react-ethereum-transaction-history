import styled from 'styled-components';

const Top = styled.div`
    border-top: 1px solid lightgray;
    border-bottom: 1px solid lightgray;
    margin-top: 100px;
    width: 100%;
    height: 40px;
    font-size: 18px;
    display: flex;
    justify-content: space-between;
    box-sizing: border-box;

`

export default Top;