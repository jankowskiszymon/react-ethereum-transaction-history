import React from 'react';
import styled from 'styled-components';

const FlexBox = styled.div`
    display: flexbox;
    justify-content: center;
    width: 100%;  
`
const StyledDiv = styled.div`
    width: 400px;
    color: white;
    background-color: #2E86AB;
    border-radius: 30px;
    margin-top: 50px;
    text-align: center;
    padding: 50px;
    a {
        font-size: 30px;
        text-decoration: none;
        color: blue;
    }
`

const About = () => {
    return (
        <FlexBox>
            <StyledDiv>
                This project was designed by Szymon Jankowski. If you want him on your board, send an e-mail to petergrynn@gmail.com. 
                I'm a young React and Ethereum developer and I'm currently looking for a job. You can use this project however you want. 
                <div>
                   <a href="https://gitlab.com/petergrynn">Gitlab</a>, <a href="https://www.linkedin.com/in/szymon-j-9947b3142/">Linkedin</a>, <a href="https://www.facebook.com/petergrynn">Facebook</a> 
                </div>
            </StyledDiv>
        </FlexBox>
    )
}

export default About;