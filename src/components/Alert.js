import React from 'react';
import styled from 'styled-components';

const Div = styled.div`
    position: fixed;
    top: 65px;
    z-index: 100;
    color: white;
    text-align: center;
    width: 100%;

`

const Alert = ({address,transaction, balance}) => (
    <Div>Your Address: {address} Transaction: {transaction} Balance of address: {balance}</Div>
)

export default Alert;