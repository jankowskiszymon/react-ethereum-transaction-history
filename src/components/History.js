import React from 'react';
import Content from './Content';
import { ethers } from 'ethers';
import HistoryEth from './HistoryEth';
import Input from './Input';
import Header from './Header';
import Info from './Info';
import Footer from './Footer';
import Alert from './Alert';
import About from './About';

class History extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            address: '',
            history: '',
            loading: false,
            balance: ''
        }
        this.change = this.change.bind(this);
        this.getHistoryOfAddress = this.getHistoryOfAddress.bind(this);
    }

    change(event){
        this.setState({
            address: event.target.value
        })

        this.getHistoryOfAddress(this.state.address)
        event.preventDefault();
    }
    
    paste(e){
        this.setState({
            address: e.clipboardData.value
        })
        e.preventDefault();
    }

    getHistoryOfAddress(address){
        if(address.length === 66){
            let wallet = new ethers.Wallet(address);

            this.state({
                address: wallet.address
            })
            this.forceUpdate();
        }

        if (address.length === 42) {
            this.setState({
                loading: true
            })
            let etherscanProvider = new ethers.providers.EtherscanProvider();
            
            etherscanProvider.getBalance(address).then((balanceOfAddress)=>{
                this.setState({
                    balance: `${ethers.utils.formatEther(balanceOfAddress)} Ether`
                })
            })
            
            console.log('adress ok');
            etherscanProvider.getHistory(address)
            .then((historyEth) => {
                this.setState({
                    history: historyEth
                })
            })
            .catch(err=> {
                console.log(`Bad address: ${err}`)
            })
            }
        else {
            console.log('address too short');
        }
    }

    render(){
        return(
            <Content>
                <Header>
                    <Input 
                        type='text' 
                        value={this.state.address} 
                        onInput={this.change}
                        placeholder="Ethereum Address or private key"
                    />
                <Alert address={this.state.address} transaction={this.state.history.length} balance={this.state.balance}/>
                </Header>
                <Info />
                {this.state.loading ? <HistoryEth history={this.state.history}/> : <About/>} 
                <Footer author="Szymon Jankowski" />
            </Content>
        )
    }
}

export default History;