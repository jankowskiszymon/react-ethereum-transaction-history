import React from 'react';
import Top from './Top';
import Paragraph from './Paragraph';


const Info = () => (
    <Top>
        ...
       <Paragraph>TxnHash:</Paragraph>
       <Paragraph>BlockNumber:</Paragraph>
       <Paragraph>Age:</Paragraph>
       <Paragraph>From:</Paragraph>
       <Paragraph>To:</Paragraph>
       <Paragraph>Value:</Paragraph>
    </Top>
)

export default Info;